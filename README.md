**Steps to setup environment for Web or API framework**
1) Clone the repository to local
2) Open the source in an IDE, preferred is Webstorm
3) run **npm install** to download all the dependencies from package.json
4) If **npm install** alone does not resolve all dependencies, try the below
      *Selenium drivers not found*::
             npm install selenium-standalone -g
             npm install selenium-webdriver -g
             npm install selenium webdriver-manager -g
      
      *rimraf is not found*::       
             npm install rimraf -g
      
      *wdio is not found*::       
             npm install -g webdriverio
             npm i wdio
             npm i wdio-config
             npm install @wdio/cli
             
      *wdio-local-runner is not found*::
             npm i @wdio/local-runner
             npm i wdio-local-runner

      *cucumber-js is not found*::
             npm install cucumber

5) Once all the packages are installed execute the below commands to ensure local repository is clean
             npm run clean
             npm run build
             
6) To execute the tests run either of the below commands
            npm run rest-test    :: This is to execute REST API tests
            npm run webui-test   :: This is to execute Web UI tests
            npm run android-test :: This is to execute mobile android tests
            npm run ios-test     :: This is to exeecute mobile ios tests

**TODO:: Add configurations for android setup
