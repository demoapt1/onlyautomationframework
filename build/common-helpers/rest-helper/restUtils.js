"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require('url-search-params-polyfill');
const assert = require('assert');
const { JSONPath } = require('jsonpath-plus');
class RestUtils {
    static setStatusCode(statcode) { this._statuscode = statcode; }
    static getStatusCode() { return this._statuscode; }
    prettyPrintJSON(json) {
        return JSON.stringify(json, null, '  ');
    }
    ;
    //validating response without giving json path
    validateResponse(response, responseFields) {
        const responsemap = responseFields.rowsHash();
        const userStr = JSON.stringify(responsemap);
        console.log('Expected response as json string' + userStr);
        let expectedvalue = false;
        JSON.parse(userStr, (key1, value1) => {
            if (value1 instanceof Object === false) {
                expectedvalue = false;
                JSON.parse(response.body, (key2, value2) => {
                    if (key1 == key2 && value1 == value2) {
                        expectedvalue = true;
                        assert.strictEqual(value1.toString(), value2.toString(), key1 + ' or ' + value1 +
                            ' is not present in response');
                    }
                });
                if (expectedvalue === false) {
                    assert.strictEqual(false, true, key1 + ' or ' + value1 +
                        ' is not present in response');
                }
            }
        });
    }
    /*Validation for json response after making http request by giving json path */
    validateJsonPathWithRegularExpression(response, responseFields) {
        const responsemap = responseFields.rowsHash();
        console.log(responseFields);
        console.log(responsemap);
        const jsons = JSON.parse(response.body);
        const userStr = JSON.stringify(responsemap);
        console.log('response fields from feature file' + userStr);
        JSON.parse(userStr, (key1, value1) => {
            console.log('JSON PATH KEY! ' + key1);
            if (value1 instanceof Object === false) {
                const result = JSONPath({ path: key1, json: jsons });
                console.log('JSON RESULT ' + result);
                if (value1.includes(',')) {
                    console.log('VALUE! ' + value1);
                    const str = value1.toString().split(',');
                    str.forEach((value) => {
                        console.log('SPLITTED ' + value);
                        console.log('RESULTSS ' + result);
                        if (result.includes(value)) {
                            assert.strictEqual(true, true, key1 + ' or ' + value +
                                ' is not present in response');
                        }
                        else {
                            assert.deepStrictEqual(false, true, key1 + ' or ' + value + ' is not present in response');
                        }
                    });
                }
                else if (result.includes(value1)) {
                    assert.strictEqual(true, true, key1 + ' or ' + value1 +
                        ' is not present in response');
                }
                else {
                    assert.deepStrictEqual(false, true, key1 + ' or ' + value1 + ' is not present in response');
                }
            }
        });
    }
    /*Validation for negative scenarios */
    invalidJsonPathWithRegularExpression(response, responseFields) {
        const responsemap = responseFields.rowsHash();
        console.log(responseFields);
        console.log(responsemap);
        const jsons = JSON.parse(response.body);
        const userStr = JSON.stringify(responsemap);
        console.log('response fields from feature file' + userStr);
        JSON.parse(userStr, (key1, value1) => {
            console.log('JSON PATH KEY! ' + key1);
            if (value1 instanceof Object === false) {
                const result = JSONPath({ path: key1, json: jsons });
                console.log('JSON RESULT ' + result);
                if (value1.includes(',')) {
                    console.log('VALUE! ' + value1);
                    const str = value1.toString().split(',');
                    str.forEach((value) => {
                        console.log('SPLITTED ' + value);
                        console.log('RESULTSS ' + result);
                        if (result.includes(value)) {
                            assert.strictEqual(true, false, key1 + ' or ' + value +
                                ' is  present in response');
                        }
                        else {
                            assert.deepStrictEqual(true, true, key1 + ' or ' + value + ' is not present in response');
                        }
                    });
                }
                else if (result.includes(value1)) {
                    assert.strictEqual(true, false, key1 + ' or ' + value1 +
                        ' is  present in response');
                }
                else {
                    assert.deepStrictEqual(true, true, key1 + ' or ' + value1 + ' is not present in response');
                }
            }
        });
    }
}
exports.RestUtils = RestUtils;
//# sourceMappingURL=restUtils.js.map