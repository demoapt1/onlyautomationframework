"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const PropertiesReader = require('properties-reader');
const properties = PropertiesReader('./config/testrail.properties');
const Testrail = require('testrail-api');
let testrail = Testrail;
class IAddTestRunImpl {
    constructor() {
        this.assignedto_id = '';
        this.description = '';
        this.include_all = true;
        this.milestone_id = '';
        this.name = '';
        this.suite_id = '';
    }
}
exports.IAddTestRunImpl = IAddTestRunImpl;
class IContentObjectImpl {
    constructor() {
        this.assignedto_id = '';
        this.comment = '';
        this.defects = '';
        this.elapsed = '';
        this.status_id = 0;
        this.version = '';
    }
}
exports.IContentObjectImpl = IContentObjectImpl;
class TestRailreport {
    constructor() {
        testrail = new Testrail({
            host: properties.get('hostname'),
            password: properties.get('password'),
            user: properties.get('username'),
        });
    }
    static set setRunId(runid) {
        TestRailreport.testRunId = runid;
    }
    static get getRunID() {
        return TestRailreport.testRunId;
    }
    async addTestResult(runid, caseid, content) {
        await testrail.addResultForCase(runid, caseid, content, (err, response, result) => {
            console.log('RESULT TTT' + err + response, result);
        });
    }
    async createTestrun(runName, suiteId) {
        //  let isrunpresent: boolean = false;
        console.log('CREAT TEST RUN');
        await testrail.addRun(/*PROJECT_ID=*/ properties.get('projectId'), /*CONTENT=*/ {
            suite_id: suiteId,
            assignedto_id: '1',
            description: runName,
            name: runName,
            include_all: true,
            case_ids: [],
            milestone_id: properties.get('milestoneId'),
        }, (err, response, run) => {
            console.log(err + response + run);
        });
        console.log('RETURN ID NEW ');
    }
    async getRunId(runName) {
        console.log('getRunID() ----------' + runName);
        await testrail.getRuns(/*PROJECT_ID=*/ properties.get('projectId'), {}, function (err, response, runs) {
            console.log('RAIL----------' + err + response + 'KEYS _______________' + runs.keys());
            for (const pair of runs.entries()) {
                for (var subpair of pair) {
                    console.log(typeof (subpair));
                    if (subpair instanceof Object == true) {
                        console.log('RUN NAME  ' + subpair.name);
                        console.log('RUN NAME ID ' + subpair.id);
                        if (runName == subpair.name) {
                            TestRailreport.testRunId = subpair.id;
                            return subpair.id;
                        }
                    }
                }
            }
        });
        return TestRailreport.testRunId;
    }
}
TestRailreport.testRunId = 0;
exports.TestRailreport = TestRailreport;
//# sourceMappingURL=testrailUtil.js.map