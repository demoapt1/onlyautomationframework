"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const protractor_1 = require("protractor");
class UiCommonHelper {
    //To select values from dropdown
    static selectDropdownByNumber(selectFilter, index) {
        selectFilter.all(protractor_1.by.tagName('option')).then(function (options) {
            options[index].click();
        });
    }
    static async selectTableRowValues(tableFilter) {
        let count;
        count = await tableFilter.all(protractor_1.by.tagName('tr')).count();
        console.log(count);
        let rowValues = '';
        for (let rowNumber = 0; rowNumber < count; rowNumber++) {
            rowValues = await tableFilter.all(protractor_1.by.xpath("//tr[" + rowNumber + "]/td")).getText();
            console.log(rowValues);
        }
        return rowValues;
    }
    static async selectTableColumnValues(tableFilter) {
        let count;
        count = await tableFilter.all(protractor_1.by.tagName('td')).count();
        console.log(count);
        let colValues = '';
        for (let colNumber = 0; colNumber < count; colNumber++) {
            colValues = await tableFilter.all(protractor_1.by.xpath("//tr/td[" + colNumber + "]")).getText();
            console.log(colValues);
        }
        return colValues;
    }
    static async presenceOfData(tableFilter, data) {
        // verify the data by getting the size of the element matches based on the text/data passed
        await tableFilter.element(protractor_1.by.xpath("//td[normalize-space(text())='" + data + "']")).click();
    }
    // get the number of rows present
    static async getRowCount(tableFilter) {
        console.log("Fetching number rows");
        return tableFilter.element(protractor_1.by.css("tr")).count();
    }
    // get the number of columns present
    getColumnCount(tableFilter) {
        return tableFilter.all(protractor_1.by.xpath("//tr[0]/td")).count();
    }
    // get the nuber of rows and columns and return it as Map
    getTableSize(tableFilter) {
        return { row: tableFilter.all(protractor_1.by.css("tr")).count(),
            columns: tableFilter.all(protractor_1.by.css("th")).count() };
    }
    // get row data and return it as list
    rowData(tableFilter, rowNumber) {
        if (rowNumber == 0) {
            throw new Error("Row number starts from 1");
        }
        rowNumber = rowNumber + 1;
        return tableFilter.all(protractor_1.by.xpath("//tr[" + rowNumber + "]/td")).getText();
    }
    // get the column data and return as list
    columnData(tableFilter, columnNumber) {
        if (columnNumber == 0) {
            throw new Error("Column number starts from 1");
        }
        columnNumber = columnNumber + 1;
        return tableFilter.all(protractor_1.by.xpath("//tr/td[" + columnNumber + "]")).getText();
    }
    // get all the data from the table
    getAllData(tableFilter) {
        return tableFilter.all(protractor_1.by.xpath("td")).getText();
    }
    // get the data from a specific cell
    getCellData(tableFilter, rowNumber, columnNumber) {
        if (rowNumber == 0) {
            throw new Error("Row number starts from 1");
        }
        rowNumber = rowNumber + 1;
        let cellData = tableFilter.element(protractor_1.by.xpath("//tr[" + rowNumber + "]/td[" + columnNumber + "]")).getText();
        return cellData;
    }
    // click checkbox with protractor
    clickCheckBox(tableFilter, data) {
        tableFilter.element(protractor_1.by.xpath("//td[normalize-space(text())='" + data + "']/..//input")).click();
    }
}
exports.UiCommonHelper = UiCommonHelper;
//# sourceMappingURL=uiHelper.js.map