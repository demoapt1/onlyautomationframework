import { by,  ElementFinder} from "protractor";


export class UiCommonHelper {

  //To select values from dropdown
  public static selectDropdownByNumber(selectFilter: ElementFinder, index: number) {
    selectFilter.all(by.tagName('option')).then(function (options) {
      options[index].click();
    });
  }

  public static async selectTableRowValues(tableFilter: ElementFinder) {

    let count: number;
    count = await tableFilter.all(by.tagName('tr')).count();
    console.log(count);
    let rowValues = '';
    for (let rowNumber: number = 0; rowNumber < count; rowNumber++) {
      rowValues = await tableFilter.all(by.xpath("//tr[" + rowNumber + "]/td")).getText();
      console.log(rowValues);

    }
    return rowValues;

  }

  public static async selectTableColumnValues(tableFilter: ElementFinder) {

    let count: number;
    count = await tableFilter.all(by.tagName('td')).count();
    console.log(count);
    let colValues ='';
    for (let colNumber: number = 0; colNumber < count; colNumber++) {
      colValues = await tableFilter.all(by.xpath("//tr/td["+colNumber+"]")).getText()
      console.log(colValues);
    }
    return colValues;

  }

  public static async presenceOfData(tableFilter: ElementFinder, data:string){
    // verify the data by getting the size of the element matches based on the text/data passed
    await tableFilter.element(by.xpath("//td[normalize-space(text())='"+data+"']")).click();
  }
  // get the number of rows present
  public static async getRowCount(tableFilter: ElementFinder){
    console.log("Fetching number rows")
    return tableFilter.element(by.css("tr")).count()
  }

  // get the number of columns present
  public getColumnCount(tableFilter: ElementFinder){

     return tableFilter.all(by.xpath("//tr[0]/td")).count()
  }

  // get the nuber of rows and columns and return it as Map
  public getTableSize(tableFilter: ElementFinder){
    return {row: tableFilter.all(by.css("tr")).count(),
      columns: tableFilter.all(by.css("th")).count()}
  }

  // get row data and return it as list
  public rowData(tableFilter: ElementFinder, rowNumber:number){
    if(rowNumber == 0){
      throw new Error("Row number starts from 1");
    }
    rowNumber = rowNumber + 1;
    return tableFilter.all(by.xpath("//tr["+rowNumber+"]/td")).getText()
  }

  // get the column data and return as list
  public columnData(tableFilter: ElementFinder ,columnNumber:number){
    if(columnNumber == 0){
      throw new Error("Column number starts from 1");
    }
    columnNumber = columnNumber + 1;
    return tableFilter.all(by.xpath("//tr/td["+columnNumber+"]")).getText()
  }

  // get all the data from the table
  public getAllData(tableFilter: ElementFinder){
    return tableFilter.all(by.xpath("td")).getText()
  }


  // get the data from a specific cell
  public getCellData(tableFilter: ElementFinder, rowNumber:number, columnNumber:number) {
    if(rowNumber == 0){
      throw new Error("Row number starts from 1");
    }
    rowNumber = rowNumber+1;
    let cellData = tableFilter.element(by.xpath
    ("//tr["+rowNumber+"]/td["+columnNumber+"]")).getText();
    return cellData;
  }
  // click checkbox with protractor
  public clickCheckBox(tableFilter: ElementFinder, data:string){
    tableFilter.element(by.xpath
    ("//td[normalize-space(text())='"+data+"']/..//input")).click()
  }


}

